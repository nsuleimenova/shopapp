//
//  ViewController.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import UIKit



protocol ViewControllerDelegate {
    func addToCart(_ item: Int)
}



class ViewController: UIViewController {

    @IBOutlet var productSwitch: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var grid: UICollectionView!
    var closes: [Product] = []
    var cart: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DatabaseConfigure()
        table.delegate = self
        table.dataSource = self
        table.isHidden = false
        grid.isHidden = true
        table.register(NewTableViewCell.nib, forCellReuseIdentifier: NewTableViewCell.indentifier)
        
        self.grid.dataSource = self
        self.grid.delegate = self
        self.grid.register(NewCollectionViewCell.nib, forCellWithReuseIdentifier: NewCollectionViewCell.indentifier)
    }
    
    @IBAction func switchTap(_ sender: Any) {
        
        if (table.isHidden){
            table.isHidden = false
            grid.isHidden = true
        }
        else{
            table.isHidden = true
            grid.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    private func DatabaseConfigure(){
        self.closes.append(Product(image_source: "ESCAN_ES690260-5", title: "ESCAN", description: "ES690260-5", price: 18100))
        self.closes.append(Product(image_source: "ESCAN_ES690261-6", title: "ESCAN", description: "ES690261-6", price: 18100))
        self.closes.append(Product(image_source: "skechers-gowalk-5-15902", title: "SKECHERS", description: "gowalk-5-15902", price: 18100))
        self.closes.append(Product(image_source: "sman-wo33405btti-bt", title: "SMAN", description: "wo33405btti-bt", price: 29900))
        self.closes.append(Product(image_source: "under-armour-hovr-sonic-3-", title: "UNDER ARMOUR", description: "hovr-sonic-3", price: 55800))
    }
    
    @IBAction func cartTap(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(identifier: CartViewController.identifier) as? CartViewController else{
                    return
            }
        vc.loved_items = self.cart
        
        navigationController?.pushViewController(vc, animated: true)
    }
}


extension ViewController: ViewControllerDelegate{
    func addToCart(_ item: Int) {
        let product = closes[item]
        cart.append(product)
    }
}


extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.closes.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: SpecificInfoViewController.identifier) as? SpecificInfoViewController else{
                    return
            }
        tableView.deselectRow(at: indexPath, animated: true)
        vc.item = self.closes[indexPath.row]
        vc.index = indexPath.row
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(150)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewTableViewCell.indentifier, for: indexPath) as! NewTableViewCell
        let close = self.closes[indexPath.row]
        cell.tableDesc.text = close.description
        cell.tableImage.image = UIImage(named: close.image_source)
        cell.tablePrice.text = String(close.price)
        cell.tableTitle.text = close.title
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
}



extension ViewController :UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.closes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = grid.dequeueReusableCell(withReuseIdentifier: NewCollectionViewCell.indentifier, for: indexPath) as! NewCollectionViewCell
        let item = self.closes[indexPath.item]
        cell.gridTitle.text = item.title
        cell.gridPrice.text = String(item.price)
        cell.gridDesc.text = item.description
        cell.gridImage.image = UIImage(named: item.image_source)
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vc = storyboard?.instantiateViewController(identifier: SpecificInfoViewController.identifier) as? SpecificInfoViewController else{
                    return
            }
        grid.deselectItem(at: indexPath, animated: true)
        vc.item = self.closes[indexPath.row]
        vc.index = indexPath.row
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension ViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.bounds.width/2
        return CGSize(width: width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
