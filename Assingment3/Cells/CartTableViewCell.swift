//
//  CartTableViewCell.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    
    static let indentifier = String(describing: CartTableViewCell.self)
    static let nib = UINib(nibName: indentifier, bundle: nil)
    
    @IBOutlet weak var cartTitle: UILabel!
    @IBOutlet weak var cartImage: UIImageView!
    @IBOutlet weak var cartDesc: UILabel!
    @IBOutlet weak var cartPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
    
}
