//
//  NewCollectionViewCell.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import UIKit

class NewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gridImage: UIImageView!
    @IBOutlet weak var gridTitle: UILabel!
    @IBOutlet weak var gridDesc: UILabel!
    @IBOutlet weak var gridPrice: UILabel!
    
    static let indentifier = String(describing: NewCollectionViewCell.self)
    static let nib = UINib(nibName: indentifier, bundle: nil)
    
    
    var index: Int = 0
    var delegate: ViewControllerDelegate?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func addGrid(_ sender: Any) {
        delegate?.addToCart(index)
    }
    
}
