//
//  NewTableViewCell.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import UIKit

class NewTableViewCell: UITableViewCell {
    
    static let indentifier = String(describing: NewTableViewCell.self)
    static let nib = UINib(nibName: indentifier, bundle: nil)
    
    
    
    
    @IBOutlet weak var tableImage: UIImageView!
    @IBOutlet weak var tableTitle: UILabel!
    @IBOutlet weak var tableDesc: UILabel!
    @IBOutlet weak var tablePrice: UILabel!
    
    
    
    var index: Int = 0
    var delegate: ViewControllerDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addToCartTap(_ sender: Any) {
        delegate?.addToCart(index)
    }
}
