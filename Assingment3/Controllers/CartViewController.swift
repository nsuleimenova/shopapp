//
//  CartViewController.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import UIKit

class CartViewController: UIViewController {
    
    var loved_items: [Product] = []
    static let identifier = String(describing: CartViewController.self)
    
    
    @IBOutlet weak var cartTable: UITableView!
    


    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        cartTable.delegate = self
        cartTable.dataSource = self
        cartTable.register(CartTableViewCell.nib, forCellReuseIdentifier: CartTableViewCell.indentifier)
    }
    
    
    @IBAction func buyTap(_ sender: Any) {
        let alert = UIAlertController(title: "Success", message: "you have bouthgt greate stuff", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
            self.loved_items = []
            self.cartTable.reloadData()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

extension CartViewController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loved_items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(130)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartTableViewCell.indentifier, for: indexPath) as! CartTableViewCell
        let item = self.loved_items[indexPath.row]
        cell.cartTitle.text = item.title
        cell.cartPrice.text = String(item.price)
        cell.cartDesc.text = item.description
        cell.cartImage.image = UIImage(named: item.image_source)
        
        
        return cell
    }
    
    
}

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


