//
//  SpecificInfoViewController.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import UIKit

class SpecificInfoViewController: UIViewController {
    
    
    @IBOutlet weak var specificImage: UIImageView!
    @IBOutlet weak var specificPrice: UILabel!
    @IBOutlet weak var specificTitle: UILabel!
    @IBOutlet weak var specificDesc: UILabel!
    
    
    
    static let identifier = String(describing: SpecificInfoViewController.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        specificImage.image = UIImage(named: item!.image_source)
        specificPrice.text = String(item!.price)
        specificDesc.text = item!.description
        specificTitle.text = item!.title
        // Do any additional setup after loading the view.
    }
    

    
    public var item: Product?
    var index: Int = 0
    var delegate: ViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func AddTap(_ sender: Any) {
        delegate?.addToCart(index)
    }
    
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
