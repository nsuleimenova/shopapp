//
//  Product.swift
//  Assingment3
//
//  Created by Nargiz Suleimenova on 07.02.2021.
//

import Foundation

struct Product {
    let image_source : String
    let title : String
    let description : String
    let price : Double
}
